#+TITLE: Attityder till kartellverksamhet

#+REVEAL_INIT_OPTIONS: width:1200, height:800, margin: 0.1, minScale:0.2, maxScale:2.5, transition:'cube'
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil
#+REVEAL_THEME: black
#+AUTHOR: Daniel Larsson
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js

* Enkäten
[[./bortfall.png]]
* 
#+ATTR_HTML: :height 600px
[[./plot1.png]]
* 
#+ATTR_HTML: :height 600px
[[./plot2.png]]
* 
#+ATTR_HTML: :height 600px
[[./plot3.png]]
* 
#+ATTR_HTML: :height 600px
[[./plot4.png]]
* 
#+ATTR_HTML: height 600px
[[./table.png]]
