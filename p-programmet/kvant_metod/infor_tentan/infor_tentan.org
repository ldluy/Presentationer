#+TITLE: Saker att tänka på
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil
#+REVEAL_THEME: moon
#+REVEAL_TRANS: cube
#+AUTHOR: Daniel Larsson
#+REVEAL_ROOT: https://cdn.jsdelivr.net/reveal.js/3.0.0/


* Analysmetod
avgörs av skalnivåer. Ni kommer långt genom att dela upp variablerna i kvalitativa och kvanitativa
* Effekter och samband
Studeras i regressioner via B-värdet. Inte via R2. 
* R^2
Är ett model fit-mått. Används främst för att se förändringar i passning när fler variabler läggs till eller när variabler manipuleras. Ett lågt R^2 indikerar att det kan finnas andra faktorer som förklarar effekten mellan beroende och oberoende variabel.
* Signifikans
Om skillnader kan sägas finnas i populationen. Inte de exakta värdena. Det går att få fram intevaller (konfidensintervaller) som anger vart värdena med t.ex. 95 % sannolikhet ligger i populationen.
