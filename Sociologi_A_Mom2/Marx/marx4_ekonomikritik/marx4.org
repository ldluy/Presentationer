#+TITLE: Marx ekonomikritik
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil
#+REVEAL_THEME: moon
#+AUTHOR: Daniel Larsson
#+REVEAL_ROOT: https://cdn.jsdelivr.net/reveal.js/3.0.0/

* Upplägg 
**** Kapitalismens minsta gemensamma nämnare      

**** Kontraktsrelationen mellan arbete och kapital 

**** Vad är kapital

**** Variabelt och Konstant kapital               

* Kapitalismens minsta gemensamma nämnare?

*** Varan 

**** Bruksvärde

**** Bytesvärde

** Hur bestäms bytesvärde

*** Genom den tid det tar att producera en produkt

*** Genom den genomsnittliga tiden det tar att producera en produkt

*** Genom den samhälleligt relaterade genomsnittliga tiden det tar att producera en produkt

*** Den genomsnittliga tiden det tar, givet teknikutveckling och organisationskonst, att producera en vara/produkt

** Vilken är den viktigaste varan?

**** Arbetskraften, dvs. förmågan att arbeta

**** Det är alltså inte arbetaren eller en arbetet som är varan, utan förmågan att arbeta

* Kontraktsrelationen mellan arbete och kapital
#+ATTR_HTML: :height 400px
[[./contract.jpg]] 

** Fair Deal?

*** ja, det är en given marknadsrelation, baserat på bytesvärdet
Men maktrelationerna är skeva

*** 
#+ATTR_HTML: :height 400px
[[./labor-vs-capital.jpg]]


*** Arbetarklassen har två former av frihet: frihet att välja arbete och frihet från egendom

* Vad är "kapitalet"?
#+ATTR_HTML: :height 400px
[[./wall-street.jpg]]

** P-V-P'

** Kapitalet är en rörelse --- en process

** Vad kommer vinsten ifrån?

** Arbetskraftens speciella egenskap

**** Kan producera mer värde än dess egna värde

**** Mervärde
Vinsten (P') = skillnaden mellan den tid det tar att skapa ett värde och tiden det tar att skapa resurser för att arbetarens reproduktion en given dag

**** Ex. åtta-timmars arbetsdag
#+ATTR_HTML: :height 200px
[[./mervarde.png]] 

*** Kan maskiner producera mervärde?
#+ATTR_HTML: :height 400px
[[./machine.jpg]]

**** Reproduktionen av en enskild maskin motsvarar alltid det värde maskinen genererar

**** Dock... maskinerna gör arbetarna effektivare...

** Hur bestäms maskiners värde

**** Den tid det tog att producera maskinen

**** Maskinen ska därför betraktas, i värdetermer, som bestående av kristalliserat arbete

**** Kristalliserat arbete äger inte samma dynamik som "levande arbete"

**** Dvs. Maskiner förslits, de reproducerar sig inte

* Variabelt och konstant kapital

** Variabelt kapital

**** kapital som har egenskap att producera mervärde --- levande arbete

** Konstant kapital

**** Maskiner, byggnader mm, kristalliserat arbete
