#+TITLE: Marx: Arbete
#+REVEAL_INIT_OPTIONS: width:1200, height:800, margin: 0.1, minScale:0.2, maxScale:2.5, transition:'cube'
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil
#+REVEAL_THEME: black
#+AUTHOR: Daniel Larsson
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
* Arbete
#+ATTR_HTML: :HEIGHT 500px
[[./arbete11.jpg]]
** Människans väsen
**** Vad skiljer människan från djuren?
** Arbete!
**  Vad är det specifika med männskligt arbete
** En särskild form av process
- Idé
- Plan
- Genomförande
- Konsumtion
*** Marx Capital (sid. 284)
"A spider conducts operations which resemble those of the weaver, and a bee would put many a human architect to shame by the construction of its honeycomb cells. But what distinguishes the worst architect from the best of bees is that the architect builds the cells in his minds before he constructs it in the wax"
** Arbetets form
*** 
#+ATTR_HTML: :height 500px
[[./quesnay.jpg]]
*** Arbete som något konkret (Fysiokraterna)
#+ATTR_HTML: :height 500px
** 
#+ATTR_HTML: :height 500px
[[./Adam_Smith.jpg]]
*** Arbete som abstrakt/generellt
*** Vad är det som bestämmer arbetets essens om det är abstrakt?
Betydelsen av denna fråga återkommer vi till 
** Filosofins slut
**** Arbete som såväl abstrakt som konkret
**** Arbete knyter filosofin till verkligheten/det konkreta
**** Filosofi blir samhällsvetenskap

* Alienation

*** Förfrämligande --- i relation till vad?

** Arbetsprodukten 

**** Är inte med på idéstadiet                                    

**** Är inte med på planeringsstadiet                             

**** Deltar i produktionen --- men bara en del av den             

**** Tar del av produktionsresultaten --- men endast en liten del 

*** Inför sig själv som människa
... med utgångspunkt i att arbetet är det som skiljer ut människan från djuren

** Konsekvenser av alienation

*** Varufetishism
 
**** Relationer förstås och uppfattas som ting
- Ex. när vi köper en iphone köper vi inte bara en materiell produkt, den inkluderar också sociala relationer baserat på produktionen av varan
**** Reifikation

*** Förväxling av "djuriska" och "mänskliga" aktiviteter

**** Vi är människor när vi borde vara djur
- exempelvis när vi äter och förökar oss

**** Vi är djur när vi borde vara människor
- när vi arbetar

*** Framträdande 

**** Överdriven kultur kring reproduktionen av arbetskraften

**** Instrumentell inställning till arbetet, samt till arbetsgemenskapen
