#+TITLE: Metodologisk individualism
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil
#+REVEAL_THEME: moon
#+REVEAL_TRANS: cube
#+AUTHOR: Daniel Larsson
#+REVEAL_ROOT: https://cdn.jsdelivr.net/reveal.js/3.0.0/
* Vad är metodologisk individualism?
** Utgångspunkt i individen
** Fokus på aktörer              
** Helheten är inte större än delarna
** Samhällsfenomen är aggregat av annat
** Ex Klass --- aggregat av yrkespositioner
* Värdeneutral
** Sociologens relation till sitt studieobjekt
** Sociologisk kunskap                        
**** ... är mer eller mindre bra observationer och betraktelser
**** Objektiv kunskap oberoende av oss är ej möjlig
** Subjektiva och värdeneutrala inslag
**** Val av fråga
**** Själva analysen
* Det specifika med vetenskapen
** Vetenskap är som religion --- systematiserad kunskap
** Vad skiljer vetenskap från religion
**** Öppen för kritik
**** Transparens    
**** Egendomsgemensam
* Nominalism
**** Om objektiv kunskap ej möjlig $->$                          
**** ... kan inte heller begrepp avspegla något objektivt $->$
**** ... då ska begrepp istället användas för att förstå         

