#+TITLE: Examinationsuppgift Utbildning, arbete och arbetsmarknad
#+OPTIONS: num:nil toc:nil html-postamble:nil author:nil
#+OPTIONS: html-style:nil html5-fancy:t
#+HTML_HEAD: <link href="notes.css" rel="stylesheet" type="text/css" />
* Introduktion
Du har i egenskap av sociologisk/socialpsykologisk utredare fått i uppdrag att skriva en introduktion till en utredning om utbildning och arbetsmarknad. Inledningen får högst vara åtta sidor och ska behandla följande frågeställningar:

1. Vad är utbildning? Redogör för utbildningens olika syften, mål och spänningsfält. (max 2 sidor)
2. Redogör för de grundläggande begreppen arbete, arbetsliv och arbetsmarknad, samt relatera de till varandra. Inkludera/lägg till ett arbetskritiskt perspektiv. (max 2 sidor)
3. Redogör för hur svensk arbetsmarknad förhåller sig i relation till den europeiska arbetsmarknaden (max 2 sidor)
4. Diskutera arbete, arbetsliv och arbetsmarknad ur ett intersektionellt perspektiv (max 3 sidor)
5. Gör en konklusion av ovanstående frågeställningar med någon form av slutsats. (max 1 sida)
* Formalia
Den rapport du skriver ska sammanlagt *inte vara längre åtta sidor*. Sidantalet är ett maxantal — rapporten får gärna vara kortare. Detta innebär att du inte kan maxa antalet sidor för varje fråga --- du måste göra ett urval utifrån prioritering du finner lämplig. Kom ihåg --- det är innehållet som bedöms — inte längden. 

Du ska använda dig av litteraturen som använts på kursen. Annan litteratur får också användas. 

Du ska besvara frågorna i löptext --- alltså inte ställa upp frågorna och svara på dem. Däremot kan du använda dig av frågorna som rubriker för att få ordning på dispositionen. 

Referenserna ska vara korrekta och utformade enligt instruktionerna i skrivguiden. Det är det ni har framför er som ni ska referera till. Rapporten ska följas av en litteraturlista. Vid användande av citat måste ni använda er av sidnummer i referensen — annars inte. Använd gärna program för att hantera referenser (t.ex. zotero.org).

- Teckensnitt: Vanlig serif (ex. liberation serif, times new roman)
- Teckenstorlek: 12 punkter
- Radavstånd: 1 1/2 rader 
- Paginera ditt arbete (dvs. sidonumrering)

Arbetet lämnas in under uppgifter i Cambro. Följ deadline. Om du missar deadline kommer ditt arbete inte att läsas förrän vid nästa utsatta deadline.

Har du frågor gällande examinationsuppgiften kontakta [[mailto:daniel.larsson@umu.se][Daniel Larsson]].


