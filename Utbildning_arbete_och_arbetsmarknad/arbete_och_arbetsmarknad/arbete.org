#+TITLE: Arbete och arbetsmarknad
#+REVEAL_INIT_OPTIONS: width:1200, height:800, margin: 0.1, minScale:0.2, maxScale:2.5, transition:'cube'
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil
#+REVEAL_THEME: black
#+AUTHOR: Daniel Larsson
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
* Varför ska vi studera arbete/arbetsmarknad?
#+ATTR_HTML: :height 400px
[[./dortmund-2011118_1920.jpg]]

* Vad är arbete?
#+ATTR_HTML: :height 400px
[[./arbete.jpg]]

** Arbete och arbetsmarknaden?
I vardagligt tal samma sak

** Former av arbete

**** Slaveri
**** Livegenskap 
**** Lönearbete (arbete som vara)
**** Egenarbete 
**** Gemensamarbete, familjearbete
* Vad är en marknad
** Tillgång och efterfrågan
* Den moderna formens arbetsliv bas och förutsättning
** Primitiv kapitalackumulation
Den dubbla friheten!
* Statute of labourers
King Edward III: 1349/1351
#+ATTR_HTML: :height 400px
[[./King_Edward_III.jpg]]
** The common land och enclosure
#+ATTR_HTML: :height 400px
[[./Riggs_at_Haddington.jpg]]

[[https://www.youtube.com/watch?v=OA4FTIz2Zrw][The diggers song]]
** Arbetsdelning och konsumtionspreferenser
* Lönearbete / Fabriksarbete 
** En helt ny form av arbete
** Tid och rums-förskjutning
** Disciplin
#+ATTR_HTML: :height 400px
[[./Panopticon.jpg]]
** Taylorism
** Fordism
Lösning på taylorismens problem?
** Fordismens kris
** Postfordism och tjänstesamhälle?
** Tilltagande arbetsdelning
Arbetskraftsbrist och arbetskraftsöverskott
* Statistik
** Arbetskraftsundersökningen
#+ATTR_HTML: :height 400px
[[./aku.png]]
** HETUS-databasen (Harmonised European Time Use Surveys)
* Saltsjöbadsavtalen
