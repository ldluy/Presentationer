#+title: Organisationskultur och makt
#+subtitle: Reflektioner kring /Kundra: Engineering Culture/
#+REVEAL_INIT_OPTIONS: width:1200, height:800, margin: 0.1, minScale:0.2, maxScale:2.5, transition:'cube'
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil
#+REVEAL_THEME: black
#+AUTHOR: Daniel Larsson
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
* Organisationskultur
Olika innebörder
**** Kännetecknande för företaget/organisationen
**** Vanor, värderingar, oskrivna regler
**** Rational över förhållningssätt och beteenden
**** Kvalitet som särskiljer
* Organisationskultur som problematiskt
Byråkratiska organisationer (Scientific Management)
**** Bottom UP
**** De anställda har en kultur -- inte företaget
**** Kultur kan vara en motkultur
* Kultur som managementstrategi
**** Kultur kan nyttjas
**** Kontroll av kroppar eller av sinnen?
**** Inre disciplin istället för yttre disciplin
**** Kultur för att påverka "självet/jaget"
* Kultur som kontroll
**** Kultur för att få medarbetare att vilja göra "rätt"
**** Internaliserad förpliktelse
**** Identifikation med företagets mål
**** inneboende tillfredsställes med arbetet
**** Arbetet är "kul"
* Kontroll
**** Från yttre kontroll till inre kontroll
**** "Normative control"
**** "You can't make them do anything, they have to want to"
**** "The culture is a mechanism of control" -- "in the minds and hearts of men[sic.]"
* Kulturen behöver en ideologi
**** Vi har unika tillvägagångssätt
**** Vi har en unik inställning
**** Vi är unika
* Kulturen behöver ritualer
**** För att få kulturen att sätta sig
**** Ritualerna "tvingar" fram "rätt" förhållningssätt och världsbild
* Motsättningar
**** Bygger på en unitaristisk syn på organisationer
**** Skillnad och sammanblandning mellan ett autentiskt jag och ett framträdande (Jmfr. Goffman)

