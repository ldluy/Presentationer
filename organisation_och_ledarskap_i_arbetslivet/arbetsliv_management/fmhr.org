#+title: Från arbetsliv till management
#+REVEAL_INIT_OPTIONS: width:1200, height:800, margin: 0.1, minScale:0.2, maxScale:2.5, transition:'cube'
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil
#+REVEAL_THEME: black
#+AUTHOR: Daniel Larsson
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js

* Industriella relationer

**** Relationen mellan arbetsgivare och anställda (och staten)
**** Fack/arbetsgivare
**** Arbetsmiljö
* Vad är ett fack
**** I sin enklaste form: en löntagarkartell
**** Grundar sig i intressen
* Intressen
**** Vad menar vi med intressen? Vart kommer de ifrån?
**** Strukturella intressen från anställda och arbetsgivare.
Är vi inte bara en säck potatis?
* Intressekonflikt arbete och kapital
**** Vem bestämmer hur något ska produceras?
**** Vem bestämmer vad som ska produceras?
**** Vem bestämmer över fördelning av produktionsresultatet?
* Parternas verktyg
**** Lock-out
**** Strejk
**** Storstrejken 1909 (lock-out)
**** Metallstrejken 1945 (strejk)
**** Storkonflikten 1980 (lock-out)
* Organisering och strejkrätt
**** Inskrivet i grundlagen
**** Del av FN's mänskliga rättigheter
* Avtal mellan organiserade arbetsgivare och facken (SAF och LO)
** Decemberkompromissen 1906:  
**** Arbetsgivarens rätt att leda och fördela arbetet
**** Arbetsgivarens rätt att anställa och avskeda
**** Fackens rätt att organisera
**** Anställdas rätt att vara med resp. inte vara med i facket
**** Rätt att träffa kollektivavtal
**** Förhandlat bort två av tre konflikter
** Lagen om kollektivavlat
**** 1928
**** Fredsplikt
**** Arbetsdomstolen (AD)
** Saltsjöbadsavtalet 1938 (den svenska modellen):
**** Samarbetsorgan (Arbetsmarknadsnämnden)
**** Förhandlingsordning
**** Uppsägning av anställda
**** Stridsåtgärder och samhällsfarliga konflikter
** Industriavtalet 1997
**** Industrinormer (märket)
* Fokus skiftar under 1980-talet från IR till management
**** Hur bör företag organiseras, styras och ledas
**** exemplifierar med personalarbete
** Personalarbetets 
**** Fabriksflickor (social arbete)
**** Personaladministration och personalvård (HR)
**** HRM: Strategiskt personalarbete, hantering av humankapital
**** SHRM (Ulrich-modellen): inkorporerat i affärsstrategin
**** Förändring av relation till facket
* Kan vi förklara denna förändringen?
**** Förändringar av arbetsmarknad och arbetsliv?
**** Värderingsförändringar?
**** Moden?
** Managementperspektivet tenderar att vara unitaristisk
**** Men det finns olika intressen i en organisation
**** Arbete-kapital
**** Arbete/familj(fritid)
**** Funktioner
**** Genussystem
**** Ras/etnicitet
** Olikheter hanteras via organisationskultur
**** Vi är en familj
**** Meritokratism: bara meriter spelar roll
** Men motsättningar sporrar motståndskulturer
** Det ekonomisk-tekniska systemets motsättningar (Lysgaard: Arbeiderkollektivet)
**** Omättligt: vilket inte människor är (kan inte arbeta hur mkt som helst)
**** Specialisering/ensidigt: men människor är mångsidiga
**** Obönhörlig/utbytbarhet: människor har behov av stabilitet
** Arbetarkollektivet
**** Buffrar för motsättningarna
**** Kulturer, normer och informella regler
* Konklusion
**** Organisationer är inte unitaristiska
**** Det finns flera typer av intressen i en organisation
**** Risk för förgivettagande
**** Vi behöver medvetandegöra intressekonflikter och förhålla oss till dem
**** Ex. via förhandlingar eller undertryckande
