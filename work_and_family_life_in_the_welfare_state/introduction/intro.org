#+TITLE: Work and Family Life in the Welfare State
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil
#+REVEAL_THEME: moon
#+REVEAL_TRANS: cube
#+AUTHOR: Daniel Larsson
#+REVEAL_ROOT: https://cdn.jsdelivr.net/reveal.js/3.0.0/
* Schedule 

Important!

[[https://www.cambro.umu.se/portal/site/22312VT19-1/tool/79418668-f03a-4f8c-a024-858e66ea14de][Changes have been made!]]
* Structure of the course
**** Lectures
- Introductions to theory and empirical evidence
- You are supposed to be active
**** Seminars
- Presentation of assignments
- Active participation: questions & comments
* Expected learning outcomes 
** On successful completion of the course, the student will have:
- knowledge about the sociological perspective on work, family, and the welfare state
- skills to demonstrate a broad understanding of key issues, themes and research areas related to work, family and the welfare state.
- On successful completion of the course, the student will have:
- ability to identify new and important research questions concerning the relationship between work, family and the welfare state
#+REVEAL:split
- ability to find and compile relevant research and distinguish between credible and non-credible information
- skills to discuss theories, arguments and concepts that are related to work, family and the welfare state, orally and in writing.
- On successful completion of the course, the student will have:
- skills to review and evaluate various theoretical and methodological perspectives to address the relevant research questions.

* Main examination
- Take-home-exam 
- Review 3 to 5 scientific papers/articles on the topic of the course
- Family and labour markets and/or welfare state responses
- max 5 pages, serif font, 1.5 line spaced
* Examination instructions
- what's the problem?
- what's the aim?
- what methods are used
- what are the results of the papers?
- own reflections: strengths, weaknesses, possible future research
* Grades
- how well the aim of the paper is framed in the introduction to the paper and whether the discussion in the paper addresses the aim of your paper
- how well you place and discuss your findings in a broader perspective and consider possible implications of your findings in the Discussion section
- how well the literature is used in the different sections of your paper

* Seminars
** Welfare state responses (group)
Posters
** Modern labour markets (individual)
A short press release
** Interdependence of labour market and family-related processes (group)
Research proposal
** Welfare state responses (group)
Presentation
** More information can be found on Cambro 





 
