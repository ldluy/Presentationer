#+TITLE: Introduktion uppsats 2024
#+subtitle: Masterprogrammet i Ledarskap och organisation

#+REVEAL_INIT_OPTIONS: width:1200, height:800, margin: 0.1, minScale:0.2, maxScale:2.5, transition:'cube'
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil
#+REVEAL_THEME: black
#+AUTHOR: Daniel Larsson
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
* Att skriva uppsats är...
**** Roligt
**** Jobbigt
**** Frustrerande
**** Svårt
**** Belönande
**** Vi klarar det! Vi har gjort det tidigare... och vi gör det tillsammans
* Teamskanaler
**** Hantering av känsliga data lagras på säker yta
**** Dokumentation om kodningsprocessen: säker yta

* Viktigt: plagiera inte!
Läs info om detta på Canvas.

Fråga om något är oklart!

* Hållpunkter
** Workshops
Är inte examinerande, utan till för att hjälpa
*** Forskningsdesign
Författare presenterar sitt arbete, läsare opponerar arbetet, kommentarer från övriga deltagare inklusive examinator.​
- Inlämning: 4:e april
- Äger rum: 8:e april
*** Halvtidsutkast
Författare presenterar sitt arbete, läsare opponerar, kommentarer från övriga deltagare inklusive examinator.
- Inlämning: 29:e april
- Äger rum: 5:e maj 
** Inlämningar
*** Granskningsseminarium
- Inlämning: 22:e maj
- Äger rum: 29:e maj
*** Slutversion
- 2:a juni
*** Programutvärdering
- 27:e maj 12:30-14:30 (Lunch)
*** Avslutningscermoni 30:e maj
*** Uppsamling
Slutet av augusti
*** DiVA-registering
Alla uppsatser ska registeras. Frivilligt att publicera.
* Gör en plan
Ta hjälp av er handeldare!
**** När ska ni göra de olika delarna i uppsatsen?
**** Tips! Lägg ner tid på introduktion, problemformulering och syfte, även om ni kommer att ändra syftet längre fram.
* Tiden går fort!
* Vetenskapligt självständigt arbete
**** Master/magisteruppsats ställer högre krav på det vetenskapliga
**** Relation till forskningsfältet & uppsatsen bidrag
** Vad innebär att det är ett vetenskaplig arbete?
***** Öppen för kritik
***** Transparent/reproducerbar
***** Egendomgemensam/communitybaserad
**** Icke-dogmatism
**** Kunskapsackumulation. Vad bidrar ni med?
** Kriterier för ett vetenskapligt arbete
**** Knyter an till ett forskningsfält
**** Teoretiska perspektiv eller antaganden
**** Väl genomförd metodavsnitt
**** Resultat med slutsatser/konklusioner
** Forskningsområdet
- Organisation
- Ledarskap
** Ni bedriver forskning -- ni är inte konsulter
** Vad menas med att bedriva forskning?
**** Samla
**** Sortera
**** Kategorisera
**** Analysera
**** Generalisera/dra slutsatser
** Problemformulering och syfte
**** Ska vara av forskningsmässigt allmänintresse
**** Forskningsfråga grundat i forskningsproblem.
**** Utgångspunkten är inte ert "case"
**** Utgångspunktet är ett fenomen
* Formalia
**** Formatmall -- vet ej om ni har en sådan
**** Typsnitt: Normal serif (t.ex. Liberation Serif, Times New Roman, Georgia)
**** Storlek: 10-12 punkter
**** Radavstånd: 1,15 -1,5 radavstånd
**** Antal sidor: 25-40
**** Referenser -- typ Harvard (om referenshanteringsprogram APA)
**** Gör en plan tidigt
* Uppsatsens struktur
**** Strukturen ska läggas upp utifrån funktion -- inte dogmatiskt
**** Ta hjälp av handledare
** Själva strukturen
**** Titel
**** Förord
**** Abstrakt/sammanfattning
**** Innehållsförteckning
**** Inledning (intro, syfte, tidigare forskning, teori)
**** Metod
**** Resultat
**** Diskussion och slutsats
**** Referenser
**** Eventuella bilagor
* Betygskriterier
