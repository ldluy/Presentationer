#+title: Tolka resultat
#+REVEAL_INIT_OPTIONS: width:1200, height:800, margin: 0.1, minScale:0.2, maxScale:2.5, transition:'cube'
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil
#+REVEAL_THEME: black
#+AUTHOR: Daniel Larsson
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
* Fokus på linjär regression

** Effektsmåttet --- det vi främst är intresserad av
#+attr_reveal: :frag (roll-in)
 * Koefficient                                   
 * Ostandardiserad beta ($\beta$)
 * $\beta =$ linjens lutning 
 * Hur mycket ökar $y$ när $x$ ökar ett skalsteg?

** Enkelt exempel (vad är feltermen?)

|    Individ | Bilar | Ålder |
|          1 |     1 |    25 |
|          2 |     4 |    43 |
|          3 |     6 |    52 |
|          4 |     3 |    34 |
|          5 |     4 |    32 |
|------------+-------+-------|
| Medelvärde |     3 |       |

** Illustrerat i graf
#+BEGIN_SRC R return tab
  bilar <- c(1, 4, 6, 3, 4)
  ålder <- c(25, 43, 52, 34, 32) 

  plot(bilar ~ ålder)
  abline(lm(bilar ~ ålder))
  summary(lm(bilar ~ ålder))

#+END_SRC
** Bilar efter ålder
[[./plot1.png]]

** Regressionslinjen
[[./plot1b.png]]

** Output
#+BEGIN_SRC R
Call:
lm(formula = bilar ~ ålder)

Residuals:
       1        2        3        4        5 
-0.67046 -0.51732  0.05925 -0.09389  1.22242 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)  
(Intercept)  -2.2835     1.5767  -1.448    0.243  
ålder         0.1582     0.0411   3.848    0.031 *
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
#+END_SRC

** Regressionslinjens element?
#+ATTR_HTML: :height 500
[[./plot1c.png]]

** Vad representerar de olika elementen
**** Estimate: \beta-koefficienten (linjens lutning)
**** Std. Error: i snitt, hur mycket skiljer sig de estimerade värdena från de observerade.
Huvudregel: Om \beta är minst dubbelt så stor statistiskt signifikant 0.05
**** t-value: Kvoten mellan \beta och Std. Error
**** P-value: huruvida resultatet är signifikant (t-test)
**** Intercept/Constant: Värdet på $y$ när $x$ är 0
**** R²: Determinationskoefficienten. Model-fit. Förklarad varians i beroende variabel
**** Adjusted R²: Justerar för antalet prediktorer/kovariater i modellen
** Vad ska vi fokusera på
#+attr_reveal: :frag (roll-in)
- Estimate
- std.error (ev.)
- /p-value/ (asterisker)
- R²
** Den linjära regressionen i tabellform
[[./table.png]]

