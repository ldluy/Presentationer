#+TITLE: Moderniteten är multidimensionell
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil
#+REVEAL_THEME: black
#+REVEAL_TRANS: cube
#+AUTHOR: Daniel Larsson
#+REVEAL_ROOT: https://cdn.jsdelivr.net/reveal.js/3.0.0/

* Modernitet är multidimensionell
** Institutionella karaktärsdrag
*** Kapitalism: 
**** Varuproduktionen är centrerad kring relationerna mellan privat ägt kapital och egendomslösa lönearbetare
**** Huvudaxeln i ett klassystem. 
**** Kapitalistisk företagsamhet bygger på konkurrens på en eller flera marknader.

*** Industrialism: 

**** Användningen av icke levande, materiella kraftkällor i produktionen av artiklar

**** Maskinens centrala roll i produktionsprocessen.

*** Komplex övervakningsapparat:  

**** Mestadels indirekt kontroll över informationsspridning. 

**** Möjliggör administrativ koncentration, vilket inte minst är viktigt för att nationalstater ska kunna fungera.

**** Överinseendet över underordnade befolkningsgruppers aktiviteter inom den politiska sfären.

*** Kontroll och koncentration över våldsmedlen
**** Militär och polis
**** monopol på våldsmedel inom ett väldefinierat territorium är utmärkande för den moderna staten. Här handlar det om


