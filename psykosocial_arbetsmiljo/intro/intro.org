#+TITLE:[[./robackskogen_1.jpg]]  
#+REVEAL_INIT_OPTIONS: width:1200, height:800, margin: 0.1, minScale:0.2, maxScale:2.5, transition:'cube'
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil author:nil
#+REVEAL_THEME: black
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js

* LÄRARE

**** Daniel Larsson

**** Lena Karlsson

**** Elin Biström

* KURSUPPLÄGG
     
** Halvfartskurs – allt görs på nätet --- inga obligatoriska träffar på campus
** Självstudier – mycket ansvar hos er som studenter
- Frivilliga/icke-obligatoriska workshops. 10-12 och 20-21.
- Kommunicera via chat/forum som finns på Canvas.
- Vi finns även att kontakta via inkorgen i Canvas.

* MOMENTEN PÅ KURSEN
- *Moment 1:* Psykosocial arbetsmiljö (7.5 hp)
- *Moment 2:* Fördjupning av psykosocial arbetsmiljö och arbetsmiljöarbete i organisationen (7,5 hp)
  
* MOMENT 1 
- *Tema 1:* Grundläggande psykosocial arbetsmiljö
- *Tema 2:* Maktförhållanden i arbetslivet
- *Tema 3:* Sociala relationer i arbetslivet (icke-obligatorisk)

** TEMA 1: GRUNDLÄGGANDE PSYKOCSOCIAL ARBETSMILJÖ
*** LITTERATUR
Eklöf, M. (2017). Psykosocial arbetsmiljö: Begrepp, bedömning och utveckling. Studentlitteratur.
*** ATT GÖRA 
- Workshop (15:e september) 
- Uppgift (2-3 sidor): Individuellt besvara ett antal frågor (finns på Canvas). (deadline: 19:e september)

** TEMA 2: MAKTFÖRHÅLLANDEN I ARBETSLIVET 
kön, klass, etnicitet

*** LITTERATUR
Acker, J. (2006). Inequality Regimes: Gender, Class, and Race in Organizations. Gender & Society, 20(4), 441–464. https://doi.org/10.1177/0891243206289499

SOU 2014:34 Inte bara jämställdhet: Intersektionella perspektiv på hinder och möjligheter i arbetslivet. (2014). Fritze. http://www.regeringen.se/sb/d/18373/a/247501

Sverke, M., Falkenberg, H., Kecklund, G., Magnusson Hansson, L., & Lindfors, P. (2016). Kvinnors och mäns arbetsvillkor: Betydelsen av organisatoriska faktorer och psykosocial arbetsmiljö för arbets- och hälsorelaterade utfall. Arbetsmiljöverket].

*** ATT GÖRA
- Workshop (5:e oktober)
- Uppgift (2-3 sidor): Individuellt besvara ett antal frågor (finns på Canvas). Deadline: 10:e oktober


** TEMA 3: SOCIALA RELATIONER I ARBETSLIVET
Kommer att se film.
*** ATT GÖRA
- Workshop 

** VG-TENTA moment 1
Kommer att finnas tillgänglig på Canvas.

Lämnas in den 25:e oktober.

* MOMENT 2
- *Tema 1:* ARBETSMILJÖ I ETT SOCIALT PERSPEKTIV
- *Tema 2:* SYSTEMATISKT ARBETMILJÖARBETE (Stress och rehabilitering, God arbetsmiljö)

** TEMA 1: ARBETSMILJÖ I ETT SOCIALT PERSPEKTIV 

*** LITTERATUR
Fagerlind Ståhl, A.-C. (2021). /Arbete Och Psykisk Hälsa: Viktigt Vetande Och Vanliga Myter/. Studentlitteratur.

Paulsen, R. (2016). /Vi Bara Lyder: En Berättelse Om Arbetsförmedlingen/. Atlas.

*** ATT GÖRA
- Workshop (17:e november)
- Uppgift (2-3 sidor): Individuellt besvara ett antal frågor (finns på Canvas). Deadline: 21:a november

*** TEMA 2: SYSTEMATISKT ARBETSMILJÖARBETE 

**** Workshop: Stress och rehablitering (8:e december).
**** Uppgift stress och rehabilitering: Individuellt besvara ett antal frågor (finns på Canvas). Deadline: 12:e december.
**** Uppgift God arbetsmiljö: individuellt besvara ett antal frågor (finns på Canvas). Deadline: 9:e januari

*** LITTERATUR

Holmström, E., & Ohlsson, K. (2014). Människan i arbetslivet: Teori och praktik. Studentlitteratur.

Systematiskt arbetsmiljöarbete [Elektronisk resurs] : Arbetsmiljöverkets föreskrifter om systematiskt arbetsmiljöarbete och allmänna råd om tillämpningen av föreskrifterna Solna : Arbetsmiljöverket : 2001 : 25 s. : 

Så förbättras verksamhetens arbetsmiljö Vägledning till Arbetsmiljöverkets föreskrifter om systematiskt arbetsmiljöarbete, AFS 2001:1 [Elektronisk resurs]
Arbetsmiljöverket : 2018-04-12T13:13:00+02:00 : 

Lindberg, P., & Vingård, E. (2012). Kunskapsöversikt: Den goda arbetsmiljön och dess indikatorer. Arbetsmiljöverket.


** VG-TENTA moment 2

Kommer att finnas tillgänglig på Canvas.

Lämnas in den 16:e januari.

