#+TITLE: Frågor till workshop om projektledningsmetodik
#+OPTIONS: num:nil toc:nil html-postamble:nil author:nil
#+OPTIONS: html-style:nil html5-fancy:t
#+HTML_HEAD: <link href="notes.css" rel="stylesheet" type="text/css" />

1. Hur ska ett projekt definieras? Vilka problem finns det med definitionen av projekt?
2. Författarna till boken skriver att projekt har blivit vanligare. Varför?
3. Projektarbete består av olika delar. Vilka är dessa?
4. Varför är dokumentation så viktigt i samband med projektarbete?
5. Har du jobbat i ett projekt? Känner du igen dig i vad som beskrivs i boken?
