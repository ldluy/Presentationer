#+TITLE: Välkommen till Projektledning
#+REVEAL_INIT_OPTIONS: width:1200, height:800, margin: 0.1, minScale:0.2, maxScale:2.5, transition:'cube'
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil
#+REVEAL_THEME: black
#+AUTHOR: Daniel Larsson
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
* Lärare
**** Daniel Larsson (Kursansvarig)
**** Emma Östin
**** Henrik Sigurdh
**** Staffan Ingmanson

* Inslag
- Inspelade föreläsningar
- Olika former av inlämningar
- Quiz
- Workshops (ej obligatoriska)
* Första uppgiften
Presentera er själva under /Diskussioner/
* Struktur
** Moment 1: Projektledningsmetodik, ledarskap och grupprocesser (6 hp)
*** Projektledningsmetodik

#+ATTR_HTML: :height 600px
[[./projektledning.jpg]]
#+REVEAL: split
**** Workshop 16:e september (15-16.30)
**** Inlämningsuppgift 1 29:e september
*** Grupp-processer
#+ATTR_HTML: :height 600px
[[./teamet.jpg]]
#+REVEAL: split
**** Workshop 14:e oktober (15-16:30)
**** Inlämningsuppgift 2 27:e oktober
*** VG-uppgift 3:e november (ej obligatorisk)
(U/G/VG)
** Moment 2: Arbetsrätt (v42) 
**** Juridiska institutionen
**** Liveföreläsning som spelas in
***** ti 26/11 10-12
***** ons 27/11 10-12
**** Quiz
#+reveal: split
#+ATTR_HTML: :height 600px
[[./arbetsratt_haftad.jpg ]]
** Moment 3: Projektanalys och projektstudie (7,5 hp)
*** Litteratur
**** Böcker
Hallin, A., & Karrbom Gustavsson, T. (2019). Projektledning. Liber.

Nilsson, A. (2008). /Projektledning i praktiken Observationer av arbete i korta projekt/. Handelshögskolan vid Umeå universitet.

#+REVEAL: split
**** Artiklar att utgå ifrån:
Blomquist, T., Hallgren, M., Nilsson, A., & Soderholm, A. (2012). Project-as-practice: In search of project management research that matters. /IEEE Engineering Management Review/, 40(3), 88–88. https://doi.org/10.1109/EMR.2012.6291583

Hällgren, M., Nilsson, A., Blomquist, T., & Söderholm, A. (2012). Relevance lost! A critical review of project management standardisation. /International Journal of Managing Projects in Business/, 5(3), 457–485. https://doi.org/10.1108/17538371211235326

Lenfle, S., Midler, C., & Hällgren, M. (2019). Exploratory Projects: From Strangeness to Theory. /Project Management Journal/, 50(5), 519–523. https://doi.org/10.1177/8756972819871781

*** Projektarbete i teori och praktik & kritiska perspektiv

**** Workshop 3: 2/12 15:00-16:30
**** Inlämningsuppgift 3: 15/12
*** Projektanalys 
**** Inlämning av film: 12/1
**** Inlämning av opposition och rapport: 19/1
* Frågor och funderingar
**** Hör av er via e-post eller via Mailfunktionen inne i Canvas (Inkorg)
**** Om frågan är av allmänintresse kan ni använda /Diskussionsfunktionen/
Skicka gärna ett mail för att uppmärksamma mig på att det finns en fråga.

