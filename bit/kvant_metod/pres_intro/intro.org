#+TITLE: Introduktion till kvantitativ metod
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil
#+REVEAL_THEME: moon
#+REVEAL_TRANS: cube
#+AUTHOR: Daniel Larsson
#+REVEAL_ROOT: https://cdn.jsdelivr.net/reveal.js/3.0.0/

* Lärare på modulen
| Daniel Larsson     | [[mailto:daniel.larsson@umu.se][daniel.larsson@umu.se]]     |
| Daniel Gabrielsson | [[mailto:daniel.gabrielsson@umu.se][daniel.gabrielsson@umu.se]] |

* Modulens upplägg
#+attr_reveal: :frag (highlight-green)
 * Fyra datalabbar
 * Två seminarier
   * Forskningsprocessen, problemformulering m.m.
   * Att analysera tabeller, och skriva resultat
   * Om begrepp
 * Föreläsningar
 * Filmer

* Cambro
Cambro och tillgång till material
* FSR

- Efter avslutat moment skall studenten avseende kunskap och förståelse:
  - känna till de vetenskapsteoretiska grunderna och etiska riktlinjerna för kvantitativ forskning inom beteende-/samhällsvetenskap
  - visa grundläggande kunskap om multivariat statistisk analys
#+reveal: split
- Efter avslutat moment skall studenten avseende färdighet och förmåga:
  - kunna göra metodologiska överväganden om kvantitativa undersökningar, om forskningsprocessen, om problemformulering samt om insamling av data
  - kunna genomföra statistiska analyser på en deskriptiv och förklarande nivå med hjälp av statistikprogrammet SPSS

* Vad är kvantitativ metod?
#+attr_reveal: :frag (roll-in)
#+ATTR_REVEAL: :frag t :frag_idx 1
*“In God we trust;* 
#+ATTR_REVEAL: :frag t :frag_idx 2 
all others bring data.”
#+ATTR_REVEAL: :frag t :frag_idx 3
W. Edwards Deming 

** Analys av data med utgångspunkt i siffror
#+attr_reveal: :frag (highlight-green)
- Matematiska kalkyler av data
- Matematiska beräkningar utifrån variablers värden
#+attr_reveal: :frag (roll-in)
*Exempel*
#+attr_reveal: :frag (roll-in) 
- Ålder (16-64)                                     
- Kön (Man, Kvinna)

* När ska vi använda kvantitativ metod?
#+ATTR_REVEAL: :frag (roll-in)
- Kartläggning
- Samband mellan faktorer
- Kausalitet

** När ska vi inte använda kvantitativ metod?
#+attr_reveal: :frag (roll-in)
- När det inte finns kvantitativ data
- När vi är intresserad av processer

** Är kvantitativ metod behäftad med ett särskilt vetenskapsteoretisk perspektiv?
#+attr_reveal: :frag (roll-in)
- Induktiv --- dvs teorigenererande
- Deduktiv --- dvs hypotestestande
- Deskription
- Korrelation/kausalitet

* Hypotestestning
#+attr_reveal: :frag (roll-in)
Kvantitativ metod innefattar nästan alltid någon form av hypotestestning

** Vad är en hypotes?
#+attr_reveal: :frag (highlight-green) 
- Ett kvalificerat antagande om ett fenomen som testas med hjälp av olika statistiska metoder.
- Utifrån teori formuleras hypotes. Operationaliseras och testas genom konkreta undersökningsfrågor med relevanta variabler.

** Hypotestestningens grundformer
#+attr_reveal: :frag (highlight-green)
H0 och H1

** Frågeställning
#+attr_reveal: :frag (roll-in)
Finns det skillnad i sökmönster mellan arbetslösa som har a-kassa och arbetslösa som inte har a-kassa?
*** 0-hyptotesen(H0): 
#+attr_reveal: :frag (highlight-green)
Det är inte någon skillnad mellan arbetslösa personer som har a-kassa eller inte har det beträffande hur selektiv de är när de söker jobb.

*** Mothypotesen (H1):
#+attr_reveal: :frag (highlight-green)
Personer som har ersättning från a-kassan när de är arbetslösa är mer selektiv (kräsna) när de söker jobb än personer som inte har ersättning från a-kassan.

* Vad för sorts data används?
#+attr_reveal: :frag (highlight-green)
- Formaliserade frågeformulär (enkät)                          
- Administrativ data                                           
- Skrapad data från databaser och/eller internet               
- Data ska vara konstruerad så att det går att göra beräkningar

* Vad är en variabler?
#+attr_reveal: :frag (highlight-green)
- Kan variera (anta olika värden)                         
- Innehåller information om den egenskap vi vill undersöka
- Tillförlitligheten är beroende av precisionen i våra operationaliseringar
- Variabler är mer eller mindre bra indikatorer

** Olika typer av variabler
#+attr_reveal: :frag (highlight-green)
- kategoriska vaiabel (kategorivariabler): anger egenskaper
- kontinuerliga variabler: Fångar verkliga värden

** Skalnivåer på variabler
#+attr_reveal: :frag (highlight-green)
- Nominalskala - kategorier med talvärden utan rangordning           
- Ordinalskala - kategorier med talvärden med rangordning            
- Intervall/kvotskala - talvärden som är rangordnade, med ekvidistans (och absolut nollpunkt gäller kvotskala)

* Centralmått
#+attr_reveal: :frag (highlight-green)
- Typvärde (mode): Det vanligast förekommande värdet
- Median (median): Mittersta observationens värde
- Medelvärde (mean) intervall och kvot. Summan av alla observationsvärden dividerat med antal observationer
  
* Centralmått och skalnivå 

| Centralmått | Skalnivå                    |
|-------------+-----------------------------|
| Typvärde    | nominal, ordinal, kvot      |
| Median      | ordinal, intervall och kvot |
| Medelvärde  | intervall och kvot          |
|-------------+-----------------------------|

* Statistisk inferens
#+ATTR_REVEAL: :frag (roll-in)
Datamaterialets beskaffenhet

#+ATTR_REVEAL: :frag (roll-in)
 * Totaldata
 * Urval
 * Obundet slumpmässigt urval
 * kvoturval
 * Ja-sägarurval

* OSU
[[./osu.png]]

