#+TITLE: Datalabb 1: Hantering av datamaterial
#+OPTIONS: html-postamble:nil author:nil
#+OPTIONS: html-style:nil html5-fancy:t

#+HTML_HEAD: <link href="notes.css" rel="stylesheet" type="text/css" />

* Dataset
Ett dataset består av flera variabler utifrån ett case. I samhällsvetenskapen är case ofta en individ. Variablerna talar om egenskapen för denna individ. Men case kan vara annat också, t.ex. länder eller bilar. När en arbetar med vetenskaplig metod är det bra att ha en känsla för matriser. Datamaterial organiseras nämligen ofta i form av matriser. 

** Mata in data
Öppna SPSS i datorn. Se till att ni är i /Data View/. Gör ett datamaterial utifrån följande tabell.

| <c>  | <c> | <c>   | <c>   | <c>     | <c> | <c> |
| Idnr | Kön | Ålder | Klass | Inkomst | Ido | Iov |
|------+-----+-------+-------+---------+-----+-----|
| 1    | 0   | 39    | 1     | 27000   | 4   | 3   |
| 2    | 1   | 18    | 1     | 18000   | 2   | 4   |
| 3    | 0   | 27    | 5     | 35000   | 8   | 9   |
| 4    | 1   | 26    | 2     | 22000   | 3   | 4   |
| 5    | 1   | 65    | 6     | 28000   | 9   | 8   |
| 6    | 0   | 33    | 2     | 24000   | 5   | 4   |
| 7    | 0   | 48    | 1     | 28000   | 3   | 1   |
| 8    | 1   | 40    | 3     | 31000   | 6   | 5   |
| 9    | 0   | 19    | 4     | 22000   | 7   | 6   |
| 10   | 1   | 20    | 4     | 21000   | 7   | 5   |
|------+-----+-------+-------+---------+-----+-----|

Ni namnger variablerna genom att ersätta “VAR001” med “idnr” osv. 

** Mata in variabelvärden
Byt därefter till /Variable View/. Klicka på /Values/ och för in värdena för /kön/ och /klass/. 

*Kön*
| <c>   | <l>    |
| Värde | Kön    |
|-------+--------|
| 0     | Kvinna |
| 1     | Man    |
|-------+--------|

*Klass:*
| <c>   | <l>                      |
| Värde | Klass                    |
|-------+--------------------------|
| 1     | Ej facklärda arbetare    |
| 2     | Facklärda arbetare       |
| 3     | Lägre tjänstemän         |
| 4     | Tjänstemän på mellannivå |
| 5     | Högre tjänstemän         |
| 6     | Företagare               |


 *Ido (inflytande över hur dagliga arbetsuppgifter organiseras):*
|----------------------------------------------------|
| Skala 0-10 (0= inget inflytande, 10=full kontroll) |
|----------------------------------------------------|


*Iov (inflytande över organisationens verksamhet):*
|----------------------------------------------------|
| Skala 0-10 (0= inget inflytande, 10=full kontroll) |
|----------------------------------------------------|


* Transfomera om datamaterialet
Alla datamaterial måste bearbetas. En del bara lite och kanske mest för att passa variablerna till den undersökning en håller på med. Andra datamaterial behöver en rejäl omgång för att överhuvudtaget fungera. Med omarbetning av datamaterial menas ofta att en gör om variablerna på ett eller annat sätt. Det kan också innebära att att strukturen på datamaterialet förändras. Med detta menas att variabel som är  /case/ byts ut. Ibland vill kanske det inte ens är önskvärt att ha ett case. Under detta moment ska vi bearbeta data via variabler.

** Recode
I denna övning ska vi börja med att transformera ålder. Klicka på /Transform/ / /Recode into different variables/. Klicka på /Ålder/ och sedan på den lilla pilen (du kan även dra variabeln in i rutan). Klicka därefter på /Old and New Variables/. Ni bör få upp en ruta som ungefär ser ut som följer:

[[./recode1.png]]

Klicka i /range/. Skriv därefter i runtan: 18. Klicka i rutan under /through/, och skriv 30. Klicka i rutan under /New Value/. Skriv 1. Klicka på /Add/.

Fortsätt på samma sätt enligt tabellen nedan.

| <c>   | <c> |
| 18-30 | 1   |
| 31-40 | 2   |
| 41-50 | 3   |
| 51-60 | 4   |
| 61-65 | 5   |

Efter att du är klar med detta tycker du på /Continue/. I rutan under /Output Variable/ anger du ett nytt variabelnamn --- förslagsvis /ålderskategorier/. Klicka på /Change/. Klocka på /OK/.

Ni kan nu se att det tillkomit en variabel (Ålderskategori). Dubbeklicka på namnet, alternativt klicka på /Variable View/. Fyll i value labels enligt:

| <c>   | <c> |
| 18-30 | 1   |
| 31-40 | 2   |
| 41-50 | 3   |
| 51-60 | 4   |
| 61-65 | 5   |

Gör en klassvariabel som skiljer mella arbetare, tjänstemän och företagare. Istället för /Range/ klickar du på /Value/. Koda om enligt följande:

| <c>             | <c>         |
| orginalvariabel | Ny variabel |
|-----------------+-------------|
| 1               | 1           |
| 2               | 1           |
| 3               | 2           |
| 4               | 2           |
| 5               | 2           |
| 6               | 3           |

Döp variabeln till något lämpligt --- t.ex klass2. Ge den nya variabeln lämpliga /variable values (variabelnamn)/.

** Syntax
Allt du gör via menyerna kan du också göra genom att skriva kod i syntax. Du kan vidare använda dig av menyerna för att skapa koder i en syntaxfil. Detta är ett bra sätt att dokumentera vad du har gjort, vilket är särskilt bra om/när något blir fel. Dessutom kan du göra mer med koder än vad som går att göra via menyerna. Ni behöver inte använda en syntax-fil, men jag skulle starkt rekommendera att ni sparar era kommandon i en sådan fil.

Gör om klassvariabeln en gång till såsom beskriver ovan. Men innan du Efter att du skrivit namnet på variabeln och klickat change, trycker du på /Paste/ istället för /OK/. Ett fönster kommer att öppna sig med följande kod:


#+BEGIN_SRC R
  RECODE  Klass
          (1 = 1) (2 = 1) (3 = 2) (4 = 2) (5 = 2) (6 = 3) 
          INTO klass2 .
  EXECUTE.
#+END_SRC 

Punkterna i koden markerar när varje enskild kod är slut. Om du ställer markören i en kodrad och trycker på den stora gröna pilen som kommer SPSS att köra koden. Det går också att trycka *Ctrl-r*. Vidare, den text ni markerar kommer också att köras vid kör-kommando.

** IF --- att koda om baserat på villkor
Tänk att vi är intresserad av att skapa en variabel baserat på två variabler. I detta fall vill vi skapa en ny variabel som särskiljer arbetarkvinnor från alla andra. Man kan göra detta via /Transfrom/Compute/. Men här ska vi använda oss av syntaxfilen. I den skriver du:

#+BEGIN_SRC R
  IF klass2 eq 1 AND Kön eq 0 klasskön = 1. 
  IF klass2 gt 1 klasskön = 2.
  IF Kön eq 1  klasskön = 2.
#+END_SRC 

Gör syntaxfönstret mindre innan du kör koderna, så att du kan se i matrisen vad som händer.

** Index
Kommandot /compute/ hjälper dig att skapa nya variabler genom att använda en funktion eller en aritmetisk operation. Detta kan vara bra när du till exempel vill skapa ett additativt index för att mer tillförlitligt mäta ett visst fenomen. 

Välj...  

#+BEGIN_SRC R 
  TRANSFORM/COMPUTE
#+END_SRC

... i rullgardinsmenyn och skriv in namnet på din nya variabel i fältet /target variable/. 

Vid fältet /Numeric expression/ skriver du in den funktion du vill använda dig av (till exempel de variabler du vill addera till ett index). Genom att trycka på knappen /Type & Label/ kan du ange en beskrivning på din variabel. 

*Exempel:* Du vill mäta Autonomi (självbestämmande) i arbetet och har bestämt dig för att variablerna ”ido” och ”iov” tillsammans mäter graden av autonomi på ett bra sätt. För att skapa ett additativt index av de två variablerna skriver du in namnet på indexvariabeln i fältet /Target variable/, du kan ex kalla variabeln för ”Autind”. För att namnge ditt index trycker du på knappen ”Type&Label” och skriver ”Autonomi index”. I fältet Numeric Expression skriver du in de två variablerna som ska adderas, i detta fall ido och iov. Sedan återstår bara att trycka ”OK” så skapas ditt index.

Innan du skapar ett index kan det dock vara bra att undersöka om indexet är bra --- eller reabelt (dvs. mäter vad som avses mätas). I ett reabelt index svarar samma individ liknande på alla ingågna variabler. I exemplet ovan är indexet bra om ido och iov hänger ihop på ett rimligt sätt. Detta kan testas genom ett Cronbach Alpha-test. Ett Cronbach-Aplha test ger ett värde som går från 0 till 1, där 1 anger perfekt korrelation, dvs. att individerna som har svarat svarar likartat på alla ingågna variabler. Ett Alpha-score över 0.65 burkar anges som OK --- men helst ska det vara högre än 0.7.

* Data från nätet
Det finns många bra datakällor på nätet, varav de flesta är helt gratis. Ofta behöver man dock registera en användare. Aggregerade data brukar dock bara vara att hämta. Med aggregerade data menas data på individnivå men som presenteras utifrån en annan enhet --- ofta land. Ett exempel är arbetslöhset (se t.ex. [[http://ec.europa.eu/eurostat/tgm/table.do?tab=table&init=1&plugin=1&pcode=tipsun20&language=en][Eurostats arbetslöhsetdata]]). Ofta behöver man rensa data ganska mycket innan den blir användbar.

Här ska vi dock använda oss av mikrodata --- alltså data på individnivå. Gå in på [[http://www.europeansocialsurvey.org][European Social Survey]] sida. Klicka på /Data and Documentation/.

[[./ess1.png]]  

Klicka på /Data and Documentation by Year/.

[[./ess2.png]]

Klicka på /More files and documents/

[[./ess3.png]]

Scrolla ner till ni hittar documentet [[http://www.europeansocialsurvey.org/docs/round8/survey/ESS8_appendix_a7_e01_0.pdf][ESS8 Appendix A7 Variables and Questions ed. 1.0]]. Ni kommer att behöva detta dokument för att veta vad variablerna betyder.

Gå sedan upp och klicka på den stora röda knappen.

[[./ess4.png]]

Klicka på /Download SPSS/.

Klicka på /New User/. Fyll i valfri men giltig e-postadress.

Gå tillbaka och skriv in e-postadressen. Klicka på /Download SPSS/ igen. Nu laddas datamaterialet ner. Lägg det på ett ställe i datorn där du finner det bekvämt. Datafilen är /Compressed/. Dubblaklicka på den för att packa upp den. Den fil som är mest intressant är /ESS8e01_1.sav/. Men läs även ReadMe-filen (gör alltid det). Öppna datamaterialet. Som du kan se är datamaterialet stort, och det ser till en början ganska rörigt ut. Så det första vi ska göra är att begränsa datamaterialet något. Börja med att stänga alla andra SPSS-filer/fönster. Därefter sparar du materialet med ett annat namn (så att vi behåller originalfilen) genom /Spara som / Save as/. Öppna ett nytt syntaxdokument (/FILE / NEW / Syntax/)

Skriv:
#+BEGIN_SRC R
  MATCH FILES
  FILE = * /
  KEEP = cntry gndr netusoft .
#+END_SRC 

Kör koden. 

Gör en frekvenstabell på länder (cntry) genom att gå till /Analyse / Descriptive Statistics / Frequnecies/. Välj Country (alt. cntry) och tryck OK (alt. paste och kör via syntaxfilen). Ni bör då få följande värden

| <l>   | <c>      | <c>  |
| CNTRY | Percent  | n    |
|-------+----------+------|
| AT    | 6.038575 | 2010 |
| BE    | 5.305534 | 1766 |
| CH    | 4.581506 | 1525 |
| CZ    | 6.909812 | 2300 |
| DE    | 8.568167 | 2852 |
| EE    | 6.065613 | 2019 |
| FI    | 5.783212 | 1925 |
| FR    | 6.218831 | 2070 |
| GB    | 5.885357 | 1959 |
| IE    | 8.309800 | 2766 |
| IL    | 7.681908 | 2557 |
| IS    | 2.643754 | 880  |
| NL    | 5.050171 | 1681 |
| NO    | 4.641591 | 1545 |
| PL    | 5.089227 | 1694 |
| RU    | 7.300367 | 2430 |
| SI    | 3.926576 | 1307 |

Det land vi är intresserad av är Storbritannien. För att göra detta kommer vi att skapa ett filter som döljer alla andra. Gå till /Data/. Välj /Select Cases/. klicka på /If conditions is satisfied/. Klicka på /if/. I den nya dialogrutan --- välj cntry. Skriv in "GB". Tryck OK. Kom ihåg att kontrollera att /Use variable Filter/ är "iklickat". Öppnda /Data view/. Nu ska det finnas / över de case som inte är med. Prova att göra en frekvenstabell över länder. Gör en frekvenstabell över kön. Gör en frekvenstabell över internetanvändande.

Öppna dokumentet med variabelbeskrivningen. Hitta variabler du finner intressanta och markera dessa för framtida bruk.
