#+TITLE: Datalabb 2: Univariat och bivariat analys
#+AUTHOR: Daniel Larsson
#+EMAIL: daniel.larsson@umu.se
#+OPTIONS: num:nil toc:nil html-postamble:nil author:nil

* Introduktion
Univariat analys används för att studera en variabel åt gången. Detta gör vi när vi vill beskriva ett fenomen, eller helt enkelt bara undersöka hur något ser ut. Ett exempel på univariat analys är hur stor andel det är som använder internet varje dag. Bivariat analys är mer av analytisk karaktär, men har fortfarande drag av deskription (alltså beskriver snarare än analysera) över sig. I bivariat analys relaterar vi en variabel gentemot en annan. Om vi tar exemplet ovan skulle det kunna vara att jämföra internetanvändandet bland män och kvinnor, eller internetanvändandet baserat på utbildning. Under denna datalabb kommer vi att återgå till den första datalabben genom att ladda ner ESS (om det inte redan finns i datamaterialet), och sedan undersöka en del variabler som finns i materialet. Vi kommer vidare utifrån undersökningen av datamaterialets variabler diskutera eventuellt behov av att tranformera om variabler.

* ESS
Börja med att ladda ner ett dataset från ESS ([[https://ldluy.gitlab.io/Presentationer/bit/kvant_metod/datalabb_1/datalabb_1.html][se instruktioner från datalabb 1]]). Detta gör du genom att gå till [[http://www.europeansocialsurvey.org][ESS hemsida]]. Väl där klickar du på /Data and Documentation/, och väljer /Data and Documentation by Year/.

Därefter klickar du på /Integrated file Round 8 (2016)/. Välj /Download SPSS/. Ange den e-postadress du angav som användare tidigare. Klicka på /Download SPSS/ igen, och filen laddas ner till datorn. Om du inte har kvar dokumentationen från förra gången behöver går till /More files and documents/. Scrolla ner till /Appendix 7/. Ladda ner PDF-filen.

Efter att du laddat ner filen dubbelklickar du på den och sparar ESS8e01.sav (den kan heta något annat, men filen ska sluta med .sav och ha 8 med i namnet). 

Om du kommer ihåg från ReadMe-filen så finns inte Sverige med i datmaterialet från 2016. Så vi kommer att välja ett annat land. Jag har valt Storbritannien. För att göra analys av Storbritannien behöver vi filtrera bort övriga länder. Detta gör du via select if-funktionen under data (se instruktioner från datalabb 1). 

* Frekvenstabeller

De variabler vi är primärt intresserade av är de variabler som berör internetanvändande. Det finns två variabler som är intressanta för ändamålet. Den ena variabeln är en kategorivariabel som ser ut som följer:

Question A 2

People can use the internet on different devices such as computers, tablets and smartphones. How often do you use the internet on these or any other devices, whether for work or personal use?

Instruction(s): Pre: CARD 1

Variable name and label: NETUSOFT Internet use, how often

Values and categories

| 1 | Never              |
| 2 | Only occasionally  |
| 3 | A few times a week |
| 4 | Most days          |
| 5 | Every day          |
| 7 | Refusal            |
| 8 | Don't know         |
| 9 | No answer          |

Det första vi vill göra är att transformera denna variabel så att den blir dikotom (skiljer på de som använder internet mycket och de som inte gör det). Det finns egentligen ingen kvalitativ gräns här (alltså där vi logiskt kan säga att det finns en skillnad), men efter visst övervägande bestämmer vi oss för att dela variabeln på följande sätt:

1 = icke-frekvent användare av internet
2 = icke-frekvent användare av internet
3 = icke-frekvent användare av internet
4 = frekvent användare av internet
5 = frekvent användare av internet

Gör denna omkodning med hjälp av recode-funktionen (se datalabb 1).

Använd 
#+BEGIN_SRC 
Recode into new variable.
#+END_SRC

Efter att du har gjort detta --- gör en frekvensanalys av variabeln. Välj lämpligt centralmått.

#+BEGIN_SRC
ANALYZE/DESCRIPTIVE STATISTICS/FREQUENCIES. 
#+END_SRC

Du bör nu ha fått upp en tabell med följande rubriker

| Value label | Value | Frequency | Percent | Valid Percent | Cum Percent |

Vad betyder dessa?

Vad kan man säga om fördelningen generellt?

* Medelvärde och median

Nästa steg är att använda den andra variablen, som lyder:

Question A 3

On a typical day, about how much time do you spend using the internet on a computer, tablet, smartphone or other device, whether for work or
personal use?

Instruction(s): Pre: ASK IF MOST DAYS OR EVERY DAY AT A2 (code 4 or 5)

Post: Please give your answer in hours and minutes.

INTERVIEWER: WRITE IN DURATION

Variable name and label: NETUSTM Internet use, how much time on typical day, in minutes. 
 
Eftersom detta är en kontinuerlig variabel passar det bäst att undersöka fördelningen med hjälp av medelvärde, median, min, max och kvartiler. Använd frekvensfunktionen för att få fram denna statistik. 

Hur många skalsteg har variabeln? Vart ligger min och max. Vart ligger medelvärdet och vart ligger medianen? Är skillnaden stor mellan median och medelvärde (vad har vi att utgå ifrån för att göra en värdering om skillnaderna mellan värdena är stora eller små)?
 
* Ålder

Nu vet vi mer om variabeln, och det kan vara dags att titta på hur andra variabler relaterar till internetanvändande. Den första variabeln vi är intresserad av är ålder. Leta upp ålder i datamaterialet (agea). Gör en frekvensanalys. Använd recode-funktionen och dela upp ålder enlligt följande:

| <r> |                    |
| 1 = | under 30 år        |
| 2 = | 31-40 år           |
| 3 = | 41-55 år           |
| 4 = | 56-65 år           |
| 5 = | 66 år eller äldre. |

Börja med att göra en frekvenstabell över den nya variabeln. Hur ser fördelningen ut?

Det är nu dags att se om det finns skillnader i ålder gällande användandet av internet. Innan vi gör det bör vi fastställa vilken variabel som är $x$ (oberoende) och vilken variabel som är $y$ (beroende). 

Efter att du gjort detta --- gå till /Analyse/Descriptive statistics/CrossTabs/. För in den beroende variabeln under rad. För sedan in den oberoende variabeln under kolumn. Tryck OK. Du har nu fått ut antalet individer i cellerna. Det vi är intresserad av är andelen i respektive ålderkategori som använder internet frekvent. Detta innebär att vi ska räkna procent. Åt vilket håll (kolumn eller rad) är det lämpligt att räkna procent?

Gå tillbaka till Crosstabs. Den gamla körningen bör ligga kvar. Tryck på *cells* och välj den väg du vill att SPSS ska räkna procent. Tryck *OK*. Vad drar du för slutsatser av analysen?

* Kön
Leta reda på variablen som anger respondenternas kön. Gör först en frekvenstabell --- hur ser fördelningen ut mellan män och kvinnor? Använd dig av Crosstab-funktionen för att undersöka vem som använder internet mest --- män eller kvinnor.

* Utbildning
Leta reda på en variabel som mäter utbildning. Koda om denna så att den har följande indelning:

- grundskola
- gymnasium
- högskola/universitet

Undersök om det finns skillnader i internetanvändandet beroende på utbildnignsnivå.

* Undersök om det går att göra ett index av inställning till invandrare

I datamaterialet finns följande variabler:

Question B 41

Would you say it is generally bad or good for [country]'s economy that people come to live here from other countries?

Instruction(s): Pre: CARD 16

Post: Please use this card.

Variable name and label: IMBGECO Immigration bad or good for country's economy

Values and categories

|    | <l>                  |
| 00 | Bad for the economy  |
| 01 | 1                    |
| 02 | 2                    |
| 03 | 3                    |
| 04 | 4                    |
| 05 | 5                    |
| 06 | 6                    |
| 07 | 7                    |
| 08 | 8                    |
| 09 | 9                    |
| 10 | Good for the economy |
| 77 | Refusal              |
| 88 | Don't know           |
| 99 | No answer            |

Question B 42 And, using this card, would you say that [country]'s cultural life is generally undermined or enriched by people coming to live here from other
countries?

Instruction(s): Pre: CARD 17

Variable name and label: IMUECLT Country's cultural life undermined or enriched by 

immigrants

Values and categories

| <r> | <l>                      |
|  00 | Cultural life undermined |
|  01 | 1                        |
|  02 | 2                        |
|  03 | 3                        |
|  04 | 4                        |
|  05 | 5                        |
|  06 | 6                        |
|  07 | 7                        |
|  08 | 8                        |
|  09 | 9                        |
|  10 | Cultural life enriched   |
|  77 | Refusal                  |
|  88 | Don't know               |
|  99 | No answer                |

Använd dig av Cronbach Alpha (finns under Analyze/reliability) för att undersöka om det är lämpligt att göra ett index av dessa två variabler. Gör ett index med hjälp av /compute-funktionen/. Fundera på vad höga värden anger och vad låga värden anger.

Extrauppgift: Fundera hur indexet kan göras om så att det går från 0 till 10. För att göra om indexet så att det går från 0 till 10 kan du experimentera med Compute-funktionen som finns under Transform. Finns det någon vinst med att transformera om indexet så att det går från 0 till 10? Finns det andra intervaller som skulle vara bättre?

* Ta reda på medelvärde och median för detta index
Använd dig av frequencies för att undersöka medelvärde och medianvärde för denna variabel. 

Använd funktionen som du hittar under:
 
#+BEGIN_SRC 
/Analyses/Compare Means/means/
#+END_SRC

för att undersöka om det finns skillnader i värde på indexet baserat på kön, ålder och utbildning.

* Brottsoffer
I datamaterialet finns en variabel som mäter brottsoffer (CRMVCT). Leta reda på denna och gör en frekvenstabell. Undersök om det finns skillnader gällande brottsoffer beroende på kön, ålder och utbildningsnivå. Fundera vidare, utifrån dokumentationen som finns, vad variabeln egentligen mäter.

* Grafer
I SPSS finns möjlighet att göra grafer. Du hittar funktioner för detta både under frequencies och under graphs. Om du har tid över --- gör grafer av de variabler du har skapat hittills.

