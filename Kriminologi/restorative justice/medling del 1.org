#+TITLE: Medling
#+subtitle: Restorative Justice

#+REVEAL_INIT_OPTIONS: width:1200, height:800, margin: 0.1, minScale:0.2, maxScale:2.5, transition:'cube'
#+OPTIONS: num:nil toc:nil html-postamble:nil timestamp:nil ^:nil
#+REVEAL_THEME: black
#+AUTHOR: Daniel Larsson
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
* Vad är medling?
**** _Frivilligt_ möte mellan gärningsman och brottsoffer
**** En medlare arrangerar och styr mötet
**** Medlaren ska vara opartisk men företräda samhället
Enligt lag: Till medlare skall en kompetent och rättrådig person utses.
Medlaren skall vara opartisk.
* Förutsättningar
**** Polisanmält
**** Erkänt brottet
**** Ska finnas ett offer/målsägande
**** Frivilligt

* Vad är syftet med medling?
**** Att komma fram till ett avtal om gottgörelse
* Medlingens status i samhället
**** Regleras i Lag (2002:445) om medling med anledning av brott 
#+ATTR_HTML: :target _blank
(https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/lag-2002445-om-medling-med-anledning-av-brott_sfs-2002-445)
**** Landets kommuner har skydlighet att ha beredskap för medling i de fall GM är under 21 år
**** Polisen har skydlighet att informera om medling
* Lagens intention
**** Att minska de negativa konsekvenserna av ett brott
**** Minska återfall i brottslighet
**** Visa GM att konflikter kan hanteras utan våld
**** BO upprättelse och svar
**** Demokratisk tanke: de drabbade hanterar brottet -- inte bara experter
**** Avlasta rättssystemet från mindre allvarliga brott
* Restorative Justice
**** Ställs emot straffande rättvisa
**** Utgångspunkt i att brott skadar och att skadan ska läkas
**** Uppgörelse och upprättelse för brottsoffret
**** Förövaren sätter sig in i brottsoffrets situation \rightarrow skam
**** Överenskommelse som ska återintegrera förövaren i samhället/gemenskapen
* Jmfr straffande rättvisa
**** Fokus på vilka lagar som brutits
**** Vem har begått brottet?
**** Vilket straff som förtjänas? (kriminologins första fråga)
