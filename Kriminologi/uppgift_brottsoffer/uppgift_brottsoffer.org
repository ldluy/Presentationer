#+TITLE: Seminarium om brottsoffer
#+OPTIONS: num:nil toc:nil html-postamble:nil author:nil
#+OPTIONS: html-style:nil html5-fancy:t
#+HTML_HEAD: <link href="notes.css" rel="stylesheet" type="text/css" />
* Inför seminariet 
** Fokusera på
*** Daigle /Victimology: the essentials/ 
**** kapitel 1: Introduction to Victimology 
**** Kapitel 2: Extent, theories, and factors of Victimization
**** Kapitel 3: Consequences of Victimization
**** Kapitel 4: Recurring Victimization
**** Kapitel 5: Victims' Rights and Remedies
**** Kapitel 6: Sexual Victimization
**** Kapitel 7: Intimate Partner Violence
**** Kapitel 11: Property and Identity Theft Victimization
*** Granström & Mannelqvist: /Brottsoffer --- rättsliga perspektiv/
**** Kapitel 1: En lärobok om brottsoffer
**** Kapitel 2: Brottsoffret i rättsprocessen
**** Kapitel 4: Välfärdsstatens ersättningar till brottsoffer
**** Kapitel 9: Brottsoffer i rätten 

* Under seminariet
Ni att delas in i fyra grupper. Varje grupp kommer att vara ansvariga för ett
av följande kapitel (4, 5, 6, 7 och 11). Under de första 45 minuterna ägnar sig gruppen åt att svara på frågorna nedan relaterat till respektive tilldelat kapitel.
Efter att detta har gjorts kommer ni att delas in i nya grupper. I dessa nya grupper kommer var och en ha ansvar att berätta om de punkter ni i ursprungsgruppen skrev ner för de andra studenterna. De andra studenterna har sedan ansvar att komplettera eller komma med frågor. Ni har sammantaget cirka 20 minuter vid varje station på er.

Kom ihåg att läsa alla kapitel som anges i uppgiften. Det kommer att öka kvaliteten på seminariet och därmed er inlärning. Uppgiftens karaktär innebär att ni inte kommer att behöva träffas innan seminariet. Men ni får gärna göra det. Till er hjälp finns numera ett forum där ni kan diskutera detta (och annat). 

Hör av er om ni har frågor. Om ni gör det, tala gärna om huruvida er fråga är allmän - så kan jag skriva till hela gruppen istället för att skicka ett enskilt mail.
**** Frågor till respektive kapitel
1. Kapitel 5: jämför hur det ser ut i USA med Sverige. Tips – Främst /Brottsoffer --- rättsliga perspektiv/, men också brottsofferjourens hemsida, samt brottsoffermyndighetens hemsida.
2. Kapitel 6, 7, 11: Svara på frågorna vad, hur, vem och varför? 
   - Vad handlar kapitlet om för brott
   - Hur förstås brottet
   - Vem utsätts (utsätter)
   - Varför utsätts vissa och hur – knyt till teori
   - Vilka är de beskrivna konsekvenser av att utsättas

Ta hjälp av /Brottsoffer ---- rättsliga perspektiv/ för att jämföra och relatera till svenska förhållanden.
