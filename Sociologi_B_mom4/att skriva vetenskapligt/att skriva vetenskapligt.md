---
author: Daniel Larsson
title: Att skriva vetenskapligt
---

```{=org}
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
```
# Vad innebär att något är vetenskapligt?

Öppen för kritik

Transparent/reproducerbar

Egendomgemensamt

# Syfte:

Icke-dogmatisk

Kunskapsackumulation

# Kriterier för ett vetenskapligt arbete

Knyter an till ett forskningsfält

Väl genomförd metodavsnitt

Resultat med konklusioner rel. till forskningsfält

# Struktur på rapporten

Introduktion

Metod

Resultat

Diskussion

## Introduktion (tratt)

Redogörelse för vad som ska undersökas

motivera varför detta är intressant att studera

Ska innehålla en problemformulering

(Ska vara relaterad till tidigare forskning)

(Kan vara relaterad till ett beteende- och/eller samhällsproblem)

Syfte: Preciserar vad som ska göras

## Data och analysmetod

Beskriv datamaterialet

1.  Hur har data samlats in?

2.  Hur många har svarat?

3.  (Vilken svarsfrekvens?)

4.  (Bortfallsanalys)

Beroende variabel

Huvudsaklig oberoende variabel/variabler

Övriga kovariater/kontrollvariabler

Analysmetoder (ex. medelvärdesanalys, regressionanalys)

Etik

## Resultat {#resultat-1}

Börja med beskrivande resultat

Därefter bivariata resultat

Slutligen multipla/multivariata resultat

## (Diskussion (uppochnedvänd tratt))

Återknyt till syfte och ev. frågeställningar

(Relatera till tidigare forskning)

Relatera till introduktionen

(Policyrelevans)

Styrkor och svagheter

Peka på forskningsbehov/förslag till framtida forskning

## Referenser

Använd referenshanterare

# Formalia

[Finns på
canvas](https://www.canvas.umu.se/courses/13462/pages/rapportuppgift?module_item_id=507105)

# Skriv enkelt

Försök att skriva i presens

Försök att skriva aktivt

-   Jag gjorde den här analys
-   Den här analysen gjordes av mig

# Röd tråd

En sak åt gången

Brygga mellan stycken och avsnitt

(undvik fragmentarisk text)

Använd logik/syllogismer

Undvik tvättlistor

Ni berättar -- även om andras forskning

Undvik CCC (Critize, Condemn, Complian)
