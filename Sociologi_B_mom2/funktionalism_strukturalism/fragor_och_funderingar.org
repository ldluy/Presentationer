#+TITLE: Frågor och funderingar
#+subtitle: Strukturalism och funktionalism
#+OPTIONS:  toc:nil num:nil html-postamble:nil author:nil ^:nil
#+OPTIONS: html-style:nil html5-fancy:t
#+HTML_HEAD: <link href="notes.css" rel="stylesheet" type="text/css" />
1. Vad är en struktur? Ta utgångspunkt i de sociala kategorier ni har läst om på modul 1. 
2. Inom strukturalismen och funktionalismen görs en poäng av särskiljandet mellan det vetenskapliga tänkandet och det vardagliga tänkandet. Utifrån det ni läst på Sociologi A modul 1 och modul 3--- vad är ett vetenskaplig tänkande, dvs. vad är vetenskap?
3. Hur kan vi veta att något är sant?
4. Problematisera _funktionen_ hos:
   - Kriminalitet
   - Arbete
   - Klass
   - Utbildning
   - Hälsningscermonier
   - Språket
   - Idrott/sport
5. Hur kan resonemang om fenomens funktionalitet relateras till vetenskapligt tänkande?
6. Funktionalismen kan ha både konservativa och progressiva drag. Hur kan det komma sig?
7. Ur ett funktionalistiskt perspektiv
   - Varför är rasism problematiskt?
   - Varför är individuell frihet bra?
8. Parson menar att en social handling är normativt reglerad och sker inom ramen för ett värdesystem. Vad menas (till vilken grad kan vi handla?)?
9. System -> Struktur -> funktion: Men hur ska vi förstå aktörer och handlande?
10. Parson menar att institutioner kan förstås som system av positioner och roller, där rollerna regleras av de _normativa förväntningar_ som riktas mot rollerna. Vilken roll har:
    - Polis
    - Utbildning
    - Ekonomin
    - Arbete/arbetsmarknad
11. Vem är det som bestämmer de normativa förväntningarna?
12. Säger ett fenomens funktion något om fenomenet i sig?
